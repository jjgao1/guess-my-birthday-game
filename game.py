from random import randint

name = input("Hi! What is your name?")

for guess_number in range (1,6):
    guess_month = randint(1,12)
    guess_year = randint (1924,2004)

    print(name, "were you born in", guess_month, "/", guess_year, "?")

    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I lost the game")
    else:
        print("Let me try again")
